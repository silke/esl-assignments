/*
 * Source:
 * https://gstreamer.freedesktop.org/documentation/application-development/basics/helloworld.html#section-helloworld
 * Copyright: Freedesktop.org
 */
#include <glib.h>
#include <gst/app/gstappsink.h>
#include <gst/gst.h>
#include <string.h>

static gboolean bus_call(GstBus *bus, GstMessage *msg, gpointer data) {
  GMainLoop *loop = (GMainLoop *)data;

  switch (GST_MESSAGE_TYPE(msg)) {

  case GST_MESSAGE_EOS:
    g_print("End of stream\n");
    g_main_loop_quit(loop);
    break;

  case GST_MESSAGE_ERROR: {
    gchar *debug;
    GError *error;

    gst_message_parse_error(msg, &error, &debug);
    g_free(debug);

    g_printerr("Error: %s\n", error->message);
    g_error_free(error);

    g_main_loop_quit(loop);
    break;
  }
  default:
    break;
  }

  return TRUE;
}

static GstFlowReturn on_sample(GstElement *elt, gpointer data) {
  g_print("Kaalbek!\n");

  GstSample *sample;
  GstFlowReturn ret;

  /* get and unref the sample from appsink */
  sample = gst_app_sink_pull_sample(GST_APP_SINK(elt));
  gst_sample_unref(sample);

  /* return OK */
  ret = GST_FLOW_OK;

  return ret;
}

int main(int argc, char *argv[]) {
  GMainLoop *loop;

  GstElement *pipeline, *source, *filter, *decoder, *sink;
  GstBus *bus;
  guint bus_watch_id;
  GstCaps *caps;

  /* Initialisation */
  gst_init(&argc, &argv);

  loop = g_main_loop_new(NULL, FALSE);

  /* Check input arguments */
  if (argc < 4) {
    g_printerr(
        "Usage: %s <source mode> <source> <destination mode> [destination options]\n"
        "  Source modes:\n"
        "    file    read from input file\n"
        "    dev     read from video4linux2 device\n\n"
        "  Destination modes:\n"
        "    file [file name]   write to file\n"
        "    app                process internally\n\n",
        argv[0]);
    return -1;
  }

  /* Create gstreamer elements */
  pipeline = gst_pipeline_new("webcam-decoder");
  filter = gst_element_factory_make("capsfilter", "video-filter");
  decoder = gst_element_factory_make("jpegdec", "jpeg-decoder");

  /* Set source according to mode */
  if (strncmp(argv[1], "file", 4) == 0) {
    source = gst_element_factory_make("filesrc", "video-source");
  } else if (strncmp(argv[1], "dev", 3) == 0) {
    source = gst_element_factory_make("v4l2src", "video-source");
  } else {
    g_printerr("Invalid source mode. Exiting.\n");
    return -1;
  }

  /* Set sink according to mode */
  if (strncmp(argv[3], "file", 4) == 0) {
    sink = gst_element_factory_make("filesink", "sink");
  } else if (strncmp(argv[3], "app", 3) == 0) {
    sink = gst_element_factory_make("appsink", "sink");
  } else {
    g_printerr("Invalid destination mode. Exiting.\n");
    return -1;
  }

  /* Check if elements were created */
  if (!pipeline) {
    g_printerr("webcam-decoder pipeline could not be created. Exiting.\n");
    return -1;
  } else if (!source) {
    g_printerr("video-source could not be created. Exiting.\n");
    return -1;
  } else if (!filter) {
    g_printerr("video-filter could not be created. Exiting.\n");
    return -1;
  } else if (!decoder) {
    g_printerr("jpeg-decoder could not be created. Exiting.\n");
    return -1;
  } else if (!sink) {
    g_printerr("file-output could not be created. Exiting.\n");
    return -1;
  }

  /* Set up the pipeline */

  /* we set the input filename to the source element */
  if (strncmp(argv[1], "file", 4) == 0) {
    g_object_set(G_OBJECT(source), "location", argv[2], NULL);
  }
  if (strncmp(argv[1], "dev", 3) == 0) {
    g_object_set(G_OBJECT(source), "device", argv[2], NULL);
  }

  /* we set the filter */
  caps = gst_caps_new_simple("image/jpeg", "framerate", GST_TYPE_FRACTION, 30,
                             1, "width", G_TYPE_INT, 640, "height", G_TYPE_INT,
                             480, NULL);
  g_object_set(G_OBJECT(filter), "caps", caps, NULL);

  /* we add a message handler */
  bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
  bus_watch_id = gst_bus_add_watch(bus, bus_call, loop);
  gst_object_unref(bus);

  /* we add all elements into the pipeline */
  /* video-source | video-filter | jpeg-decoder | sink */
  gst_bin_add_many(GST_BIN(pipeline), source, filter, decoder, sink, NULL);

  /* we link the elements together */
  /* video-source -> video-filter ~> jpeg-decoder -> sink */
  gst_element_link_many(source, filter, decoder, sink, NULL);

  /* Set the output file */
  if (strncmp(argv[3], "file", 4) == 0) {
    g_object_set(G_OBJECT(sink), "location", argv[4], NULL);
  }

  /* Set callback on new sample */
  if (strncmp(argv[3], "app", 3) == 0) {
    g_object_set(G_OBJECT(sink), "emit-signals", TRUE, "sync", FALSE, NULL);
    g_signal_connect(sink, "new-sample", G_CALLBACK(on_sample), NULL);
  }

  /* Set the pipeline to "playing" state*/
  g_print("Now playing: %s\n", argv[1]);
  gst_element_set_state(pipeline, GST_STATE_PLAYING);

  /* Iterate */
  g_print("Running...\n");
  g_main_loop_run(loop);

  /* Out of the main loop, clean up nicely */
  g_print("Returned, stopping playback\n");
  gst_element_set_state(pipeline, GST_STATE_NULL);

  g_print("Deleting pipeline\n");
  gst_object_unref(GST_OBJECT(pipeline));
  g_source_remove(bus_watch_id);
  g_main_loop_unref(loop);

  return 0;
}
