/**
 * @file example.c
 * @brief c example file.
 * @author Jan Jaap Kempenaar, University of Twente.
 * @author Silke Hofstra, Koen Zandberg, Universiteit Twente.
 */

#include "gpmc_driver_c.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <fcntl.h>  // open()
#include <unistd.h> // close()

// Some settings
#define TESTCOUNT 1000000
#define DEBUG 0

// Terminal colors
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

int test(long in, long out) {
  return in == out << 1;
}

int main(int argc, char *argv[]) {
  int fd; // File descriptor.
  long in, out; // Values to read and write
  int pass = 1; // Test status


  if (2 != argc) {
    printf("Usage: %s <device_name>\n", argv[0]);
    return 1;
  }

  printf("GPMC driver c-example\n");

  // open connection to device.
  printf("Opening gpmc_fpga...\n");
  fd = open(argv[1], 0);
  if (0 > fd) {
    printf("Error, could not open device: %s.\n", argv[1]);
    return 1;
  }

  // Seed the random number generator
  srand(time(NULL));

  // Get start time
  clock_t begin = clock();

  // Run a program
  for(int i = 0; i < TESTCOUNT; i++) {
    // Write a random number to idx 4
    out = rand();
    setGPMCValue(fd, out, 4);

    // Read result from idx 0
    in = getGPMCValue(fd, 0);

    // Show result
    if(!test(in,out)) {
      printf("Check %03i: " KRED "fail:" KNRM " 2*%ld != %ld\n", i, out, in);
      pass = 0;
    } else if(DEBUG) {
      printf("Check %03i: " KGRN "pass\n" KNRM, i);
    }
  }

  // Get end time
  clock_t end = clock();
  double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

  // Debug output
  if(DEBUG) {
    printf("Status:\n");
    for(int j = 0; j < 32; j=j+2) {
      printf("Reg %02i = %ld\n", j, getGPMCValue(fd, j));
    }
  }

  // Test status
  if(pass) {
    printf(KGRN "All tests passed\n" KNRM);
  } else {
    printf(KRED "One or more tests failed\n" KNRM);
  }

  // Show execution time
  printf("Execution took %.3f seconds (%.3f MiB/s)\n", time_spent, (TESTCOUNT * 8)/time_spent/pow(2,20));

  // close connection to free resources.
  close(fd);
  return 0;
}
