--
-- @file setup_control.vhd
-- @brief Toplevel file template file which can be used as a reference for implementing gpmc communication.
-- @author Jan Jaap Kempenaar, University of Twente 2014
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity setup_control is
  generic(
    DATA_WIDTH           : integer := 16;
    GPMC_ADDR_WIDTH_HIGH : integer := 10;
    GPMC_ADDR_WIDTH_LOW  : integer := 1;
    -- RAM_SIZE should be a power of 2
    RAM_SIZE             : integer := 64
    );
  port (
    CLOCK_50 : in std_logic;

    -- GPMC side
    GPMC_DATA     : inout std_logic_vector(DATA_WIDTH - 1 downto 0);
    GPMC_ADDR     : in    std_logic_vector(GPMC_ADDR_WIDTH_HIGH downto GPMC_ADDR_WIDTH_LOW);
    GPMC_nPWE     : in    std_logic;
    GPMC_nOE      : in    std_logic;
    GPMC_FPGA_IRQ : in    std_logic;
    GPMC_nCS6     : in    std_logic;
    GPMC_CLK      : in    std_logic;

    -- FPGA side
    -- Output headers
    F_IN  : in  std_logic_vector(15 downto 0);
    F_OUT : out std_logic_vector(15 downto 0);

    -- Pulse width modulators
    PWM1A : out std_logic;
    PWM1B : out std_logic;
    PWM1C : out std_logic;

    PWM2A : out std_logic;
    PWM2B : out std_logic;
    PWM2C : out std_logic;

    PWM3A : out std_logic;
    PWM3B : out std_logic;
    PWM3C : out std_logic;

    PWM4A : out std_logic;
    PWM4B : out std_logic;
    PWM4C : out std_logic;

    -- Encoders
    ENC1A : in std_logic;
    ENC1B : in std_logic;
    ENC1I : in std_logic;

    ENC2A : in std_logic;
    ENC2B : in std_logic;
    ENC2I : in std_logic;

    ENC3A : in std_logic;
    ENC3B : in std_logic;
    ENC3I : in std_logic;

    ENC4A : in std_logic;
    ENC4B : in std_logic;
    ENC4I : in std_logic
    );
end setup_control;


architecture structure of setup_control is
  -- GPMC controller component for FPGA
  component ramstix_gpmc_driver is
    generic(
      DATA_WIDTH           : integer := 16;
      GPMC_ADDR_WIDTH_HIGH : integer := 10;
      GPMC_ADDR_WIDTH_LOW  : integer := 1;
      RAM_SIZE             : integer := 32
      );
    port(
      clk : in std_logic;

      -- Data from FPGA to Overo Fire (input for GPMC driver)
      reg0_in : in std_logic_vector(DATA_WIDTH - 1 downto 0);
      reg1_in : in std_logic_vector(DATA_WIDTH - 1 downto 0);
      reg2_in : in std_logic_vector(DATA_WIDTH - 1 downto 0);
      reg3_in : in std_logic_vector(DATA_WIDTH - 1 downto 0);

      -- Data from Overo Fire to FPGA (output of GPMC driver)
      reg4_out : out std_logic_vector(DATA_WIDTH - 1 downto 0);
      reg5_out : out std_logic_vector(DATA_WIDTH - 1 downto 0);
      reg6_out : out std_logic_vector(DATA_WIDTH - 1 downto 0);
      reg7_out : out std_logic_vector(DATA_WIDTH - 1 downto 0);

      GPMC_DATA     : inout std_logic_vector(DATA_WIDTH - 1 downto 0);
      GPMC_ADDR     : in    std_logic_vector(GPMC_ADDR_WIDTH_HIGH downto GPMC_ADDR_WIDTH_LOW);
      GPMC_nPWE     : in    std_logic;
      GPMC_nOE      : in    std_logic;
      GPMC_FPGA_IRQ : in    std_logic;
      GPMC_nCS6     : in    std_logic;
      GPMC_CLK      : in    std_logic
      );

  end component;

  -- Define signals to connect the component to the gpmc_driver
  signal buffer_out    : std_logic_vector(2*DATA_WIDTH - 1 downto 0);
  signal buffer_in     : std_logic_vector(2*DATA_WIDTH - 1 downto 0);
  signal temp_out      : std_logic_vector(2*DATA_WIDTH - 1 downto 0);
  alias msb_buffer_out : std_logic_vector(DATA_WIDTH - 1 downto 0) is buffer_out(2*DATA_WIDTH - 1 downto DATA_WIDTH);
  alias lsb_buffer_out : std_logic_vector(DATA_WIDTH - 1 downto 0) is buffer_out(DATA_WIDTH - 1 downto 0);
  alias msb_buffer_in  : std_logic_vector(DATA_WIDTH - 1 downto 0) is buffer_in(2*DATA_WIDTH - 1 downto DATA_WIDTH);
  alias lsb_buffer_in  : std_logic_vector(DATA_WIDTH - 1 downto 0) is buffer_in(DATA_WIDTH - 1 downto 0);
  alias msb_temp_out   : std_logic_vector(DATA_WIDTH - 1 downto 0) is temp_out(2*DATA_WIDTH - 1 downto DATA_WIDTH);
  alias lsb_temp_out   : std_logic_vector(DATA_WIDTH - 1 downto 0) is temp_out(DATA_WIDTH - 1 downto 0);

  constant zero : std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '0');
begin
  -- Map GPMC controller to I/O.
  gpmc_driver : ramstix_gpmc_driver generic map(
    DATA_WIDTH           => DATA_WIDTH,
    GPMC_ADDR_WIDTH_HIGH => GPMC_ADDR_WIDTH_HIGH,
    GPMC_ADDR_WIDTH_LOW  => GPMC_ADDR_WIDTH_LOW,
    RAM_SIZE             => RAM_SIZE
    )
    port map (
      clk     => CLOCK_50,
      -- Linux offset: idx 0
      reg0_in => lsb_buffer_in,         -- LSB
      reg1_in => msb_buffer_in,         -- MSB

      -- Linux offset: idx 2
      reg2_in => zero,                  -- LSB
      reg3_in => zero,                  -- MSB

      -- Linux offset: idx 4
      reg4_out => lsb_buffer_out,       -- LSB
      reg5_out => msb_buffer_out,       -- MSB

      -- Linux offset: idx 6
      reg6_out => lsb_temp_out,         -- LSB
      reg7_out => msb_temp_out,         -- MSB

      GPMC_DATA     => GPMC_DATA,
      GPMC_ADDR     => GPMC_ADDR,
      GPMC_nPWE     => GPMC_nPWE,
      GPMC_nOE      => GPMC_nOE,
      GPMC_FPGA_IRQ => GPMC_FPGA_IRQ,
      GPMC_nCS6     => GPMC_nCS6,
      GPMC_CLK      => GPMC_CLK
      );

  -- Test application
  buffer_in <= buffer_out(2*DATA_WIDTH-2 downto 0) & '0';
end architecture;
