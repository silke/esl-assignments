
#include "count_binary.h"
#include "io.h"

static void count_led()
{
#ifdef LED_PIO_BASE
    IOWR_ALTERA_AVALON_PIO_DATA(
        LED_PIO_BASE,
        IORD(QUADRATURE_DEC_0_BASE, 0)
        );
#endif
}

static void count_all()
{
    count_led();
    printf("Encoder: %ld\n", IORD(QUADRATURE_DEC_0_BASE, 0));
}
  
int main(void)
{ 
	IOWR(QUADRATURE_DEC_0_BASE, 0, 200);
    while( 1 ) 
    {
        usleep(100000);
        count_all();
    }
    return 0;
}
