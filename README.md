Embedded Systems Lab assignments
================================

Planning
--------

| Section   | Weeks | Assignments |
|-----------|-------|-------------|
| 2 Intro's | 1-2   | 1-2         |
| 3 Blocks  | 3-5   | 3-8         |
| 4 Combine | 6     | 9-10        |
| 5 Control | 7     | 11-12       |
| 6 DSE     | 8     | 13          |
| 6 Demo    | 9     | 14          |
