library vunit_lib;
library ieee;
library osvvm;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use osvvm.RandomPkg.all;

entity tb_quad_decoder is
  generic (runner_cfg : string  := "";
           data_width : natural := 16;
           num_tests  : natural := 100);
end tb_quad_decoder;

architecture tester of tb_quad_decoder is

  component quad_decoder is
    generic (BUS_WIDTH : integer);
    port(rst, clk       : in  std_logic;
         phase1, phase2 : in  std_ulogic;
         quad_out       : out signed(BUS_WIDTH downto 1);
         quad_in        : in  signed(BUS_WIDTH downto 1);
         write_en       : in  std_ulogic;
         changed        : out std_logic);
  end component;

  subtype bit2 is std_ulogic_vector (1 downto 0);

  constant bus_width         : integer             := 32;
  constant low               : bit2                := "00";
  constant right_high        : bit2                := "01";
  constant high              : bit2                := "11";
  constant left_high         : bit2                := "10";
  signal   state             : bit2                := low;
  signal   clk, reset, write : std_logic           := '0';
  signal   quad              : signed(data_width-1 downto 0);
  signal   newquad           : signed(data_width-1 downto 0) := (others => '0');
  signal   irq               : std_logic;

begin
  reset <= '0', '1' after 100 ns;
  clk   <= not clk  after 10 ns;
  qd : quad_decoder
    generic map (BUS_WIDTH => data_width)
    port map(reset,
             clk,
             state(1),
             state(0),
             quad,
             newquad,
             write,
             irq);

  main : process
    variable RV0, RV1, RV2 : RandomPType;
    variable counter       : signed(data_width-1 downto 0);
    variable change        : integer range -1 to 1;
    variable loopcount     : integer;
    variable looptime      : time;

    -- Increment the quadrature state
    procedure increment is
    begin
      case state is
        when low        => state <= right_high;
        when right_high => state <= high;
        when high       => state <= left_high;
        when left_high  => state <= low;
        when others     =>
      end case;
    end procedure;

    -- decrement the quadrature state
    procedure decrement is
    begin
      case state is
        when low        => state <= left_high;
        when right_high => state <= low;
        when high       => state <= right_high;
        when left_high  => state <= high;
        when others     =>
      end case;
    end procedure;

  begin
    -- Simulation start
    test_runner_setup(runner, runner_cfg);

    -- Seed the random values
    RV0.InitSeed (RV0'instance_name);
    RV1.InitSeed (RV1'instance_name);
    RV2.InitSeed (RV2'instance_name);

    -- Wait until the reset is done
    wait for 100 ns;

    -- Set new value and wait for update tick
    newquad <= ('1', others => '0');
    write   <= '1';
    wait until clk = '1';
    write   <= '0';
    wait until clk = '0';
    wait until clk = '1';

    -- Check initialisation values
    check_equal(newquad, quad, "New value does not match set value", failure);

    -- Initialise counter
    counter := quad;

    -- Test the quad_decoder with 5000 random situations
    for i in 0 to num_tests loop
      -- Generate random values for the change (increment, decrement or nothing),
      -- number of changes and time that the loop takes
      change    := RV0.RandInt(-1, 1);
      loopcount := RV1.RandInt(1, 200);
      looptime  := RV2.RandTime(60 ns, 1000 ns);

      -- Perform a number of changes
      for I in 0 to loopcount loop
        -- Select the kind of change
        case change is
          when 0  =>
          when 1  => increment;
          when -1 => decrement;
        end case;

        -- Wait for the set interval
        wait for looptime;

        -- Check if the predicted value is the same as the decoded value
        check_equal(counter + change, quad, "New value does not match incremented value", failure);

        -- Set counter to decoded value
        counter := quad;
      end loop;
    end loop;

    -- Simulation ends here
    test_runner_cleanup(runner);
  end process;
end tester;
