-- GPMC bus driver  
-- Twente University 2013
-- 
-- Please refer to http://www.ti.com/lit/ug/spruf98x/spruf98x.pdf for the Technical Reference Manual of 
-- the OMAP35x processor, where the GPMC interface is fully explained (Ch. 11.1).
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ramstix_gpmc_driver is
  generic(
    DATA_WIDTH           : integer := 16;
    
    GPMC_ADDR_WIDTH_HIGH : integer := 10;
    GPMC_ADDR_WIDTH_LOW  : integer := 1;
    -- RAM_SIZE should be a power of 2
    RAM_SIZE             : integer := 1024
  );
  port(
    clk           : in    std_logic;
    reset         : in    std_logic;
    GPMC_data     : inout std_logic_vector(DATA_WIDTH - 1 downto 0);
    GPMC_addr     : in    std_logic_vector(GPMC_ADDR_WIDTH_HIGH downto GPMC_ADDR_WIDTH_LOW);
    GPMC_npwe     : in    std_logic;
    GPMC_noe      : in    std_logic;
    GPMC_fpga_irq : in    std_logic;
    GPMC_ncs6     : in    std_logic;
    GPMC_clk      : in    std_logic;
    
    -- Data signal i/o
    -- Tilt signals
    t_kp, t_ki, t_kd, t_lim : out signed(31 downto 0);
    t_encoder_input        : in signed(31 downto 0);
	 t_encoder_en           : out std_logic;
    t_encoder_output       : out signed(31 downto 0);
    t_pwm_top, t_pwm_lead  : out signed(31 downto 0);
    t_setpoint             : out signed(31 downto 0);
    
    -- Pan signals
    p_kp, p_ki, p_kd, p_lim : out signed(31 downto 0);
    p_encoder_input         : in signed(31 downto 0);
    p_encoder_output        : out signed(31 downto 0);
	 p_encoder_en            : out std_logic;
    p_pwm_top, p_pwm_lead   : out signed(31 downto 0);
    p_setpoint              : out signed(31 downto 0)

  );

end ramstix_gpmc_driver;

architecture behavior of ramstix_gpmc_driver is
  constant REG_WIDTH      : integer := 2*DATA_WIDTH;
  constant REG_HALF       : integer := REG_WIDTH/2;
  constant zero           : signed(REG_HALF-1 downto 0) := (others => '0');
  -- data_out register, which will be only active if both the chip-select and read-enable are negative
  signal gpmc_data_out : std_logic_vector(15 downto 0);

  -- register set
  type mem_type is array (0 to RAM_SIZE - 1) of std_logic_vector(DATA_WIDTH - 1 downto 0);
  signal ram : mem_type;

  signal gpmc_data_in : std_logic_vector(DATA_WIDTH - 1 downto 0)                             := (others => '0');
  signal gpmc_addr_in : std_logic_vector(GPMC_ADDR_WIDTH_HIGH - GPMC_ADDR_WIDTH_LOW downto 0) := (others => '0');
  signal gpmc_ncs     : std_logic                                                             := '0';
  signal gpmc_nwe     : std_logic                                                             := '0';
  signal gpmc_nre     : std_logic                                                             := '0';

  constant reg_t_kp_loc : integer := 24;
  signal reg_t_kp      : signed(REG_WIDTH-1 downto 0);
  alias  reg_t_kp_high : signed(REG_HALF-1 downto 0) is reg_t_kp(REG_WIDTH-1 downto REG_HALF);
  alias  reg_t_kp_low  : signed(REG_HALF-1 downto 0) is reg_t_kp(REG_HALF-1 downto 0);
  
  constant reg_t_ki_loc : integer := 28;
  signal reg_t_ki      : signed(REG_WIDTH-1 downto 0);
  alias  reg_t_ki_high : signed(REG_HALF-1 downto 0) is reg_t_ki(REG_WIDTH-1 downto REG_HALF);
  alias  reg_t_ki_low  : signed(REG_HALF-1 downto 0) is reg_t_ki(REG_HALF-1 downto 0);
  
  constant reg_t_kd_loc : integer := 32;
  signal reg_t_kd      : signed(REG_WIDTH-1 downto 0);
  alias  reg_t_kd_high : signed(REG_HALF-1 downto 0) is reg_t_kd(REG_WIDTH-1 downto REG_HALF);
  alias  reg_t_kd_low  : signed(REG_HALF-1 downto 0) is reg_t_kd(REG_HALF-1 downto 0);
  
  constant reg_t_lim_loc : integer := 36;
  signal reg_t_lim      : signed(REG_WIDTH-1 downto 0);
  alias  reg_t_lim_high : signed(REG_HALF-1 downto 0) is reg_t_lim(REG_WIDTH-1 downto REG_HALF);
  alias  reg_t_lim_low  : signed(REG_HALF-1 downto 0) is reg_t_lim(REG_HALF-1 downto 0);
  
  constant reg_t_enc_loc : integer := 40;
  signal reg_t_encoder_output      : signed(REG_WIDTH-1 downto 0);
  alias  reg_t_encoder_output_high : signed(REG_HALF-1 downto 0) is reg_t_encoder_output(REG_WIDTH-1 downto REG_HALF);
  alias  reg_t_encoder_output_low  : signed(REG_HALF-1 downto 0) is reg_t_encoder_output(REG_HALF-1 downto 0);
  
  constant reg_t_pwm_top_loc : integer := 44;
  signal reg_t_pwm_top      : signed(REG_WIDTH-1 downto 0);
  alias  reg_t_pwm_top_high : signed(REG_HALF-1 downto 0) is reg_t_pwm_top(REG_WIDTH-1 downto REG_HALF);
  alias  reg_t_pwm_top_low  : signed(REG_HALF-1 downto 0) is reg_t_pwm_top(REG_HALF-1 downto 0);
  
  constant reg_t_pwm_lead_loc : integer := 48;
  signal reg_t_pwm_lead      : signed(REG_WIDTH-1 downto 0);
  alias  reg_t_pwm_lead_high : signed(REG_HALF-1 downto 0) is reg_t_pwm_lead(REG_WIDTH-1 downto REG_HALF);
  alias  reg_t_pwm_lead_low  : signed(REG_HALF-1 downto 0) is reg_t_pwm_lead(REG_HALF-1 downto 0);
  
  constant reg_t_setpoint_loc : integer := 52;
  signal reg_t_setpoint      : signed(REG_WIDTH-1 downto 0);
  alias  reg_t_setpoint_high : signed(REG_HALF-1 downto 0) is reg_t_setpoint(REG_WIDTH-1 downto REG_HALF);
  alias  reg_t_setpoint_low  : signed(REG_HALF-1 downto 0) is reg_t_setpoint(REG_HALF-1 downto 0);
  
  constant reg_p_kp_loc : integer := 64;
  signal reg_p_kp      : signed(REG_WIDTH-1 downto 0);
  alias  reg_p_kp_high : signed(REG_HALF-1 downto 0) is reg_p_kp(REG_WIDTH-1 downto REG_HALF);
  alias  reg_p_kp_low  : signed(REG_HALF-1 downto 0) is reg_p_kp(REG_HALF-1 downto 0);
  
  constant reg_p_ki_loc : integer := 68;
  signal reg_p_ki      : signed(REG_WIDTH-1 downto 0);
  alias  reg_p_ki_high : signed(REG_HALF-1 downto 0) is reg_p_ki(REG_WIDTH-1 downto REG_HALF);
  alias  reg_p_ki_low  : signed(REG_HALF-1 downto 0) is reg_p_ki(REG_HALF-1 downto 0);
  
  constant reg_p_kd_loc : integer := 72;
  signal reg_p_kd      : signed(REG_WIDTH-1 downto 0);
  alias  reg_p_kd_high : signed(REG_HALF-1 downto 0) is reg_p_kd(REG_WIDTH-1 downto REG_HALF);
  alias  reg_p_kd_low  : signed(REG_HALF-1 downto 0) is reg_p_kd(REG_HALF-1 downto 0);
  
  constant reg_p_lim_loc : integer := 76;
  signal reg_p_lim      : signed(REG_WIDTH-1 downto 0);
  alias  reg_p_lim_high : signed(REG_HALF-1 downto 0) is reg_p_lim(REG_WIDTH-1 downto REG_HALF);
  alias  reg_p_lim_low  : signed(REG_HALF-1 downto 0) is reg_p_lim(REG_HALF-1 downto 0);
  
  constant reg_p_enc_loc : integer := 80;
  signal reg_p_encoder_output      : signed(REG_WIDTH-1 downto 0);
  alias  reg_p_encoder_output_high : signed(REG_HALF-1 downto 0) is reg_p_encoder_output(REG_WIDTH-1 downto REG_HALF);
  alias  reg_p_encoder_output_low  : signed(REG_HALF-1 downto 0) is reg_p_encoder_output(REG_HALF-1 downto 0);
  
  constant reg_p_pwm_top_loc : integer := 84;
  signal reg_p_pwm_top      : signed(REG_WIDTH-1 downto 0);
  alias  reg_p_pwm_top_high : signed(REG_HALF-1 downto 0) is reg_p_pwm_top(REG_WIDTH-1 downto REG_HALF);
  alias  reg_p_pwm_top_low  : signed(REG_HALF-1 downto 0) is reg_p_pwm_top(REG_HALF-1 downto 0);
  
  constant reg_p_pwm_lead_loc : integer := 88;
  signal reg_p_pwm_lead      : signed(REG_WIDTH-1 downto 0);
  alias  reg_p_pwm_lead_high : signed(REG_HALF-1 downto 0) is reg_p_pwm_lead(REG_WIDTH-1 downto REG_HALF);
  alias  reg_p_pwm_lead_low  : signed(REG_HALF-1 downto 0) is reg_p_pwm_lead(REG_HALF-1 downto 0);
  
  constant reg_p_setpoint_loc : integer := 92;
  signal reg_p_setpoint      : signed(REG_WIDTH-1 downto 0);
  alias  reg_p_setpoint_high : signed(REG_HALF-1 downto 0) is reg_p_setpoint(REG_WIDTH-1 downto REG_HALF);
  alias  reg_p_setpoint_low  : signed(REG_HALF-1 downto 0) is reg_p_setpoint(REG_HALF-1 downto 0);
begin
  process_input : process(clk)
  begin
    if (rising_edge(clk)) then
      gpmc_data_in <= GPMC_data;
      gpmc_addr_in <= GPMC_addr;
      gpmc_ncs     <= GPMC_ncs6;
      gpmc_nwe     <= GPMC_npwe;
      gpmc_nre     <= GPMC_noe;
    end if;
  end process process_input;

  process_gpmc : process(clk, reset, GPMC_ncs6, GPMC_data, gpmc_addr_in)
    variable gpmc_ram_addr : integer;
    variable p_enc_write, t_enc_write : std_logic;
  begin
    -- A negative chipselect (GPMC_nCS6) indicates a read- or write operation on the GPMC bus.

    gpmc_ram_addr := to_integer(unsigned(gpmc_addr_in));

    if reset = '0' then
      reg_t_kp_low <= zero;
      reg_t_kp_high <= zero;
      reg_t_ki_low <= zero;
      reg_t_ki_high <= zero;
      reg_t_kd_low <= zero;
      reg_t_kd_high <= zero;
      reg_t_lim_low <= zero;
      reg_t_lim_high <= zero;
      reg_t_encoder_output_low <= zero;
      reg_t_encoder_output_high <= zero;
      reg_t_pwm_top_low <= zero;
      reg_t_pwm_top_high <= zero;
      reg_t_pwm_lead_low <= zero;
      reg_t_pwm_lead_high <= zero;
      reg_t_setpoint_low <= zero;
      reg_t_setpoint_high <= zero;
      reg_p_kp_low <= zero;
      reg_p_kp_high <= zero;
      reg_p_ki_low <= zero;
      reg_p_ki_high <= zero;
      reg_p_kd_low <= zero;
      reg_p_kd_high <= zero;
      reg_p_lim_low <= zero;
      reg_p_lim_high <= zero;
      reg_p_pwm_top_low <= zero;
      reg_p_pwm_top_high <= zero;
      reg_p_pwm_lead_low <= zero;
      reg_p_pwm_lead_high <= zero;
      reg_p_setpoint_low <= zero;
      reg_p_setpoint_high <= zero;
	elsif (rising_edge(clk)) then
		p_enc_write := '0';
	   t_enc_write := '0';
      if (gpmc_ncs = '0') then
        if (gpmc_nwe = '0') then
          if (gpmc_ram_addr < RAM_SIZE) then      
            ram(gpmc_ram_addr) <= gpmc_data_in;
          else
			   if (gpmc_ram_addr = reg_t_enc_loc) or (gpmc_ram_addr = reg_t_enc_loc + 1) then
					t_enc_write := '1';
				end if;
				if (gpmc_ram_addr = reg_p_enc_loc) or (gpmc_ram_addr = reg_p_enc_loc + 1) then
					p_enc_write := '1';
				end if;
            -- Set special configuration registers
            case gpmc_ram_addr is
              when reg_t_kp_loc           => reg_t_kp_low <= signed(gpmc_data_in);
              when reg_t_kp_loc + 1       => reg_t_kp_high <= signed(gpmc_data_in);
              when reg_t_ki_loc           => reg_t_ki_low <= signed(gpmc_data_in);
              when reg_t_ki_loc + 1       => reg_t_ki_high <= signed(gpmc_data_in);
              when reg_t_kd_loc           => reg_t_kd_low <= signed(gpmc_data_in);
              when reg_t_kd_loc + 1       => reg_t_kd_high <= signed(gpmc_data_in);
              when reg_t_lim_loc          => reg_t_lim_low <= signed(gpmc_data_in);
              when reg_t_lim_loc + 1      => reg_t_lim_high <= signed(gpmc_data_in);
              when reg_t_enc_loc          => reg_t_encoder_output_low <= signed(gpmc_data_in);
              when reg_t_enc_loc + 1      => reg_t_encoder_output_high <= signed(gpmc_data_in);
              when reg_t_pwm_top_loc      => reg_t_pwm_top_low <= signed(gpmc_data_in);
              when reg_t_pwm_top_loc + 1  => reg_t_pwm_top_high <= signed(gpmc_data_in);
              when reg_t_pwm_lead_loc     => reg_t_pwm_lead_low <= signed(gpmc_data_in);
              when reg_t_pwm_lead_loc + 1 => reg_t_pwm_lead_high <= signed(gpmc_data_in);
              when reg_t_setpoint_loc     => reg_t_setpoint_low <= signed(gpmc_data_in);
              when reg_t_setpoint_loc + 1 => reg_t_setpoint_high <= signed(gpmc_data_in);
              when reg_p_kp_loc           => reg_p_kp_low <= signed(gpmc_data_in);
              when reg_p_kp_loc + 1       => reg_p_kp_high <= signed(gpmc_data_in);
              when reg_p_ki_loc           => reg_p_ki_low <= signed(gpmc_data_in);
              when reg_p_ki_loc + 1       => reg_p_ki_high <= signed(gpmc_data_in);
              when reg_p_kd_loc           => reg_p_kd_low <= signed(gpmc_data_in);
              when reg_p_kd_loc + 1       => reg_p_kd_high <= signed(gpmc_data_in);
              when reg_p_lim_loc          => reg_p_lim_low <= signed(gpmc_data_in);
              when reg_p_lim_loc + 1      => reg_p_lim_high <= signed(gpmc_data_in);
              when reg_p_enc_loc          => reg_p_encoder_output_low <= signed(gpmc_data_in);
              when reg_p_enc_loc + 1      => reg_p_encoder_output_high <= signed(gpmc_data_in);
              when reg_p_pwm_top_loc      => reg_p_pwm_top_low <= signed(gpmc_data_in);
              when reg_p_pwm_top_loc + 1  => reg_p_pwm_top_high <= signed(gpmc_data_in);
              when reg_p_pwm_lead_loc     => reg_p_pwm_lead_low <= signed(gpmc_data_in);
              when reg_p_pwm_lead_loc + 1 => reg_p_pwm_lead_high <= signed(gpmc_data_in);
              when reg_p_setpoint_loc     => reg_p_setpoint_low <= signed(gpmc_data_in);
              when reg_p_setpoint_loc + 1 => reg_p_setpoint_high <= signed(gpmc_data_in);
              when others =>
            end case;
          end if;
        elsif (gpmc_nre = '0') then
          if (gpmc_ram_addr < RAM_SIZE) then
            gpmc_data_out <= ram(gpmc_ram_addr);
          else
            -- Read special configuration register
            case gpmc_ram_addr is
              when reg_t_kp_loc                 => gpmc_data_out <= std_logic_vector(reg_t_kp_low);
              when reg_t_kp_loc + 1             => gpmc_data_out <= std_logic_vector(reg_t_kp_high);
              when reg_t_ki_loc                 => gpmc_data_out <= std_logic_vector(reg_t_ki_low);
              when reg_t_ki_loc + 1             => gpmc_data_out <= std_logic_vector(reg_t_ki_high);
              when reg_t_kd_loc                 => gpmc_data_out <= std_logic_vector(reg_t_kd_low);
              when reg_t_kd_loc + 1             => gpmc_data_out <= std_logic_vector(reg_t_kd_high);
              when reg_t_lim_loc                => gpmc_data_out <= std_logic_vector(reg_t_lim_low);
              when reg_t_lim_loc + 1            => gpmc_data_out <= std_logic_vector(reg_t_lim_high);
              when reg_t_enc_loc      => gpmc_data_out <= std_logic_vector(t_encoder_input(REG_HALF-1 downto 0));
              when reg_t_enc_loc + 1  => gpmc_data_out <= std_logic_vector(t_encoder_input(REG_WIDTH-1 downto REG_HALF));
              when reg_t_pwm_top_loc            => gpmc_data_out <= std_logic_vector(reg_t_pwm_top_low);
              when reg_t_pwm_top_loc + 1        => gpmc_data_out <= std_logic_vector(reg_t_pwm_top_high);
              when reg_t_pwm_lead_loc           => gpmc_data_out <= std_logic_vector(reg_t_pwm_lead_low);
              when reg_t_pwm_lead_loc + 1       => gpmc_data_out <= std_logic_vector(reg_t_pwm_lead_high);
              when reg_t_setpoint_loc           => gpmc_data_out <= std_logic_vector(reg_t_setpoint_low);
              when reg_t_setpoint_loc + 1       => gpmc_data_out <= std_logic_vector(reg_t_setpoint_high);
              when reg_p_kp_loc                 => gpmc_data_out <= std_logic_vector(reg_p_kp_low);
              when reg_p_kp_loc + 1             => gpmc_data_out <= std_logic_vector(reg_p_kp_high);
              when reg_p_ki_loc                 => gpmc_data_out <= std_logic_vector(reg_p_ki_low);
              when reg_p_ki_loc + 1             => gpmc_data_out <= std_logic_vector(reg_p_ki_high);
              when reg_p_kd_loc                 => gpmc_data_out <= std_logic_vector(reg_p_kd_low);
              when reg_p_kd_loc + 1             => gpmc_data_out <= std_logic_vector(reg_p_kd_high);
              when reg_p_lim_loc                => gpmc_data_out <= std_logic_vector(reg_p_lim_low);
              when reg_p_lim_loc + 1            => gpmc_data_out <= std_logic_vector(reg_p_lim_high);
              when reg_p_enc_loc     => gpmc_data_out <= std_logic_vector(p_encoder_input(REG_HALF-1 downto 0));
              when reg_p_enc_loc + 1 => gpmc_data_out <= std_logic_vector(p_encoder_input(REG_WIDTH-1 downto REG_HALF));
              when reg_p_pwm_top_loc            => gpmc_data_out <= std_logic_vector(reg_p_pwm_top_low);
              when reg_p_pwm_top_loc + 1        => gpmc_data_out <= std_logic_vector(reg_p_pwm_top_high);
              when reg_p_pwm_lead_loc           => gpmc_data_out <= std_logic_vector(reg_p_pwm_lead_low);
              when reg_p_pwm_lead_loc + 1       => gpmc_data_out <= std_logic_vector(reg_p_pwm_lead_high);
              when reg_p_setpoint_loc           => gpmc_data_out <= std_logic_vector(reg_p_setpoint_low);
              when reg_p_setpoint_loc + 1       => gpmc_data_out <= std_logic_vector(reg_p_setpoint_high);
              when others => gpmc_data_out <= x"0000";
            end case;
          end if;
        end if;
      end if;
    end if;
    t_encoder_en <= t_enc_write;
	 p_encoder_en <= p_enc_write;
  end process process_gpmc;
  t_kp <= reg_t_kp;
  t_ki <= reg_t_ki;
  t_kd <= reg_t_kd;
  t_lim <= reg_t_lim;
  t_pwm_top <= reg_t_pwm_top;
  t_pwm_lead <= reg_t_pwm_lead;
  t_setpoint <= reg_t_setpoint;
  t_encoder_output <= reg_t_encoder_output;
  p_kp <= reg_p_kp;
  p_ki <= reg_p_ki;
  p_kd <= reg_p_kd;
  p_lim <= reg_p_lim;
  p_pwm_top <= reg_p_pwm_top;
  p_pwm_lead <= reg_p_pwm_lead;
  p_setpoint <= reg_p_setpoint;
  p_encoder_output <= reg_p_encoder_output;

  -- Place data on the bus only if both the chip-select and read-enable are negative
  GPMC_DATA <= gpmc_data_out when (GPMC_ncs6 = '0' AND GPMC_noe = '0') else (others => 'Z');

end;
